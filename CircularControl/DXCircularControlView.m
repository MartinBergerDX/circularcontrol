//
//  DXCircularControlView.m
//  CircularControl
//
//  Created by Mac on 2/8/15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

// nastavi: content krece da se pojavljuje iz gorenjeg desnog ugla

#import <QuartzCore/QuartzCore.h>
#import "DXCircularControlView.h"
#import "DXCircularControlLayer.h"
#import "Helper.h"

@interface DXCircularControlView ()
@property (strong, nonatomic) CALayer *containerLayer;
@property (strong, nonatomic) DXCircularControlLayer *circularControlLayer;
@end

@implementation DXCircularControlView

- (id) init
{
    if (self = [super init])
    {
        [self commonInitDXCircularControlView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)aRect
{
    if (self = [super initWithFrame:aRect])
    {
        [self commonInitDXCircularControlView];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self commonInitDXCircularControlView];
    }
    return self;
}

- (void) commonInitDXCircularControlView
{
    self.circularControlLayer = [[DXCircularControlLayer alloc] init];
    
    DXCircularControlLayer *circularControlLayer = self.circularControlLayer;
    if ([self.circularControlLayer respondsToSelector:@selector(setContentsScale:)])
    {
        [self.circularControlLayer setContentsScale:[[UIScreen mainScreen] scale]];
    }
    circularControlLayer.circleLineWidth = 4.0f;
    circularControlLayer.circleRadius = 50.0f;
    //circularControlLayer.circleAngle = M_PI_2;
    circularControlLayer.circleColor = [UIColor blueColor];

    //circularControlLayer.frame = CGRectMake(0, 0, 100, 100);
    
    self.containerLayer = [[CALayer alloc] init];
    self.containerLayer.frame = self.bounds;
    [self.containerLayer addSublayer:circularControlLayer];
    [self.layer addSublayer:self.containerLayer];
    
    [self setNeedsDisplay];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
}

- (void) setAngle:(CGFloat)angle
{
    self.circularControlLayer.frame = self.bounds;
    self.circularControlLayer.circleAngle = angle;
}



#pragma mark - UIControl

- (BOOL) beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super beginTrackingWithTouch:touch withEvent:event];
    //NSLog(@"%s", __func__);
    CGPoint p1 = [touch locationInView:self];
    NSLog(@"%6.2f, %6.2f", p1.x, p1.y);
    return YES;
}

- (BOOL) continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super continueTrackingWithTouch:touch withEvent:event];
    //NSLog(@"%s", __func__);
    CGPoint p1 = [touch locationInView:self];
    NSLog(@"%6.2f, %6.2f", p1.x, p1.y);
    return YES;
}

- (void) endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super endTrackingWithTouch:touch withEvent:event];
    //NSLog(@"%s", __func__);
}

@end
