//
//  DXCircularControl.m
//  CircularControl
//
//  Created by Mac on 2/7/15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#import "DXCircularControlLayer.h"
#import "Helper.h"

static NSString * const DXCircularControlLayerPropertyNameAngle = @"circleAngle";
static NSString * const DXCircularControlLayerPropertyNameRadius = @"circleRadius";
static NSString * const DXCircularControlLayerPropertyNameLineWidth = @"circleLineWidth";
static const CGFloat DXAnimationDuration = 0.5f;

@implementation DXCircularControlLayer

@dynamic circleAngle;

- (CABasicAnimation*) makeAnimationForKey:(NSString*)key
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:key];

    NSLog(@"%s, angle: %6.2f", __func__, self.circleAngle);
    
    //animation.fromValue = [[self presentationLayer] valueForKey:key];
    animation.fromValue = @0;
    
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    animation.duration = DXAnimationDuration;
    
    return animation;
}

- (instancetype) init
{
    if (self = [super init])
    {
        // TODO: query the delegate for values
        [self setNeedsDisplay];
    }
    
    return self;
}

- (id<CAAction>) actionForKey:(NSString *)event
{
    if ([event isEqualToString:DXCircularControlLayerPropertyNameAngle])
    {
        return [self makeAnimationForKey:event];
    }
    return [super actionForKey:event];
}

+ (BOOL)needsDisplayForKey:(NSString *)key
{
    if ([key isEqualToString:DXCircularControlLayerPropertyNameAngle])
    {
        return YES;
    }
    return [super needsDisplayForKey:key];
}

- (void) drawInContext:(CGContextRef)ctx
{
    id modelLayer = [self modelLayer];
    CGFloat radius = [[modelLayer valueForKey:DXCircularControlLayerPropertyNameRadius] floatValue];
    CGFloat lineWidth = [[modelLayer valueForKey:DXCircularControlLayerPropertyNameLineWidth] floatValue];
    //NSLog(@"%s, angle: %6.2f, radius: %6.2f, angle_m: %6.2f, radius_m: %6.2f", __func__, self.circleAngle, self.circleRadius, [[modelLayer valueForKey:@"circleAngle"] floatValue], [[modelLayer valueForKey:@"circleRadius"] floatValue]);
    
    // draw circle arc up to target angle
    
    CGRect frame = self.frame;
    CGContextRef context = ctx;
    
    CGContextSetShouldAntialias(context, YES);
    CGContextSetAllowsAntialiasing(context, YES);
    
    // draw thin circle
    
    //CGContextSetLineWidth(context, <#CGFloat width#>)
    
    // draw selection circle
    
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetStrokeColorWithColor(context, [UIColor greenColor].CGColor);
    CGContextAddArc(context, frame.size.width / 2.0f, frame.size.height / 2.0f, radius, 0.0f, self.circleAngle, 0);
    
    CGContextStrokePath(context);
    
    CGContextSetShouldAntialias(context, NO);

}

@end
