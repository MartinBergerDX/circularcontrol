//
//  Helper.h
//  CircularControl
//
//  Created by Mac on 2/8/15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#ifndef CircularControl_Helper_h
#define CircularControl_Helper_h

#import <Foundation/Foundation.h>

static void printRect(CGRect rect)
{
#ifdef DEBUG
    NSLog(@"%6.2f, %6.2f, %6.2f, %6.2f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
#endif
}

static float deg2rad(float degree)
{
    return degree * (M_PI / 180.0f);
}

static float rad2deg(float radian)
{
    return radian * (180.0f / M_PI);
}

#endif
