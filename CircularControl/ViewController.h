//
//  ViewController.h
//  CircularControl
//
//  Created by Mac on 2/7/15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXCircularControlView.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet DXCircularControlView *circularControlView;
- (IBAction)actionFire:(id)sender;

@end

