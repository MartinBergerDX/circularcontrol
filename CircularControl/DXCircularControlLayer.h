//
//  DXCircularControl.h
//  CircularControl
//
//  Created by Mac on 2/7/15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@protocol DXCircularControlLayerDelegate <NSObject>

@end

@interface DXCircularControlLayer : CALayer

@property (nonatomic) CGFloat thinCircleRadius;
@property (nonatomic) CGFloat thinCircleLineWidth;
@property (strong, nonatomic) UIColor *thinCircleColor;

@property (nonatomic) CGFloat circleRadius;
@property (nonatomic) CGFloat circleAngle;
@property (nonatomic) CGFloat circleLineWidth;
@property (strong, nonatomic) UIColor *circleColor;

@property (nonatomic) BOOL headVisible;
@property (nonatomic) CGFloat headRadius;
@property (strong, nonatomic) UIColor *headColor;

@end
