//
//  DXCircularControlView.h
//  CircularControl
//
//  Created by Mac on 2/8/15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DXCircularControlView : UIControl

@property (nonatomic) CGFloat angle;

// animates circle to "angle"
- (void) animate;

@end

