//
//  TrigHelpers.c
//  CircularControl
//
//  Created by Martin Berger on 4/20/15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#include "TrigHelpers.h"
#include <math.h>

int pointInsideCircle(float point_x, float point_y, float circle_x, float circle_y, float circle_radius)
{
    // (x - center_x)^2 + (y - center_y)^2 < radius^2
    return (sqrtf(point_x - circle_x) + sqrtf(point_y - circle_y)) < sqrtf(circle_radius);
}