//
//  TrigHelpers.h
//  CircularControl
//
//  Created by Martin Berger on 4/20/15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#ifndef __CircularControl__TrigHelpers__
#define __CircularControl__TrigHelpers__

#include <stdio.h>

int pointInsideCircle(float point_x, float point_y, float circle_x, float circle_y, float circle_radius);

#endif /* defined(__CircularControl__TrigHelpers__) */
