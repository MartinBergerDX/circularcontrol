//
//  ViewController.m
//  CircularControl
//
//  Created by Mac on 2/7/15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#import "ViewController.h"
#import "DXCircularControlView.h"
#import "Helper.h"

@interface ViewController ()

//@property (strong, nonatomic) DXCircularControlView *circularControlView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //self.circularControlView = [[DXCircularControlView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    //[self.view addSubview:self.circularControlView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionFire:(id)sender
{
    //[self.circularControlView setAngle:M_PI_2];
    //return;
    
    CGFloat angleInRadians = deg2rad(arc4random() % 360);
    NSLog(@"angle: %6.2f", angleInRadians);
    NSAssert(angleInRadians >= 0.0f && angleInRadians <= M_PI * 2.0f, @"bad angle: %6.2f", angleInRadians);
    [self.circularControlView setAngle:angleInRadians];
}

@end
